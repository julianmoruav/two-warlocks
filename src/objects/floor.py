import pygame as pg

class Floor(pg.sprite.Sprite):
    """ Class for the floor of the map """

    def __init__(self, x, y):
        super().__init__()
        self.name = 'F'
        self.image = pg.image.load('../assets/map/floor.bmp')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self):
        pass