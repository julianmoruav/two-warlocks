import pygame as pg

class Wall(pg.sprite.Sprite):
    """ Class for the wall that limits the map """

    def __init__(self, x, y, is_visible):
        super().__init__()
        self.name = 'W'
        self.is_visible = is_visible
        self.image = pg.image.load('../assets/map/wall.bmp')
        self.rect = self.image.get_rect()
        if not is_visible:
            self.image.fill((0, 0, 0))
        self.rect.x = x
        self.rect.y = y

    def update(self):
        pass