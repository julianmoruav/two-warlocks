import pygame as pg

class Door(pg.sprite.Sprite):
    """ Class for the door on the wall """

    def __init__(self, x, y, is_locked):
        super().__init__()
        self.name = 'D'
        self.is_locked = is_locked
        self.sprites = {
            'locked': pg.image.load('../assets/map/locked_door.bmp'),
            'unlocked': pg.image.load('../assets/map/unlocked_door.bmp')
        }
        if is_locked:
            self.image = self.sprites['locked']
        else:
            self.image = self.sprites['unlocked']

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def unlock(self):
        self.is_locked = False

    def update(self):
        if self.is_locked:
            self.image = self.sprites['locked']
        else:
            self.image = self.sprites['unlocked']