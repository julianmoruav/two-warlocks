from game.globals import *
import pygame as pg

class Character(pg.sprite.Sprite):
    ''' Class for a player '''

    def __init__(self, name, sprites_dir, pos):
        super().__init__()
        self.name = name
        self.sprites = self.load_sprites(sprites_dir)
        self.face_direction = 'down'
        self.current_sprite = 0
        self.image = self.sprites[self.face_direction][self.current_sprite]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.is_moving = False
        self.current_animation_step = 0
        self.max_animation_steps = 8
        self.speed = 4
        self.is_dead = False

    def load_sprites(self, directory: str):
        sprites_dict = dict()
        for direction in sprites_files:
            sprites = list()
            for sp in sprites_files[direction]:
                try:
                    sprites.append( pg.image.load(f'{directory}/{sp}') )
                except:
                    pass
            sprites_dict[direction] = sprites
        return sprites_dict

    def step(self, step_number):
        # change the active image sprite
        num_sprites = len(self.sprites[self.face_direction])
        next_step = (step_number + 1) % num_sprites
        self.image = self.sprites[self.face_direction][next_step]

        # change the rect location
        if self.face_direction == "up":
            self.rect.y -= self.speed    
        elif self.face_direction == "down":
            self.rect.y += self.speed
        elif self.face_direction == "left":
            self.rect.x -= self.speed
        elif self.face_direction == "right":
            self.rect.x += self.speed    

    def move(self, direction: str):
        self.is_moving = True
        self.face_direction = direction
        self.current_animation_step = 0
        
    def update(self):
        if self.is_moving:
            if self.current_animation_step < self.max_animation_steps:
                self.current_animation_step += 1
                self.step(self.current_animation_step)
            else:
                self.is_moving = False
                self.image = self.sprites[self.face_direction][0]
        elif self.is_dead:
            self.image = self.sprites["dead"][0]

    def get_neighbor_coords(self):
        image_w = self.image.get_width()
        image_h = self.image.get_height()
        mid_x = self.rect.x + image_w/2
        mid_y = self.rect.y + image_h/2
        return {
            "up": (mid_x, mid_y - TILE_SIZE),
            "down": (mid_x, mid_y + TILE_SIZE),
            "left": (mid_x - TILE_SIZE, mid_y),
            "right": (mid_x + TILE_SIZE, mid_y),
        }
        
    def die(self):
        self.is_dead = True

class Alice(Character):
    def __init__(self, x, y):
        super().__init__(
            name = 'A',
            sprites_dir = '../assets/alice',
            pos = (x+4, y+4) # +4 is a workaround to center sprite on floor
        )

class Bob(Character):
    def __init__(self, x, y):
        super().__init__(
            name = 'B',
            sprites_dir = '../assets/bob',
            pos = (x+4, y+4) # +4 is a workaround to center sprite on floor
        )

class Zombie(Character):
    def __init__(self, x, y):
        super().__init__(
            name = 'Z',
            sprites_dir = '../assets/zombie',
            pos = (x+4, y+4) # +4 is a workaround to center sprite on floor
        )