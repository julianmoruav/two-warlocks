import pygame as pg

class Life(pg.sprite.Sprite):

    def __init__(self, x, y, val=3):
        super().__init__()
        self.lives = val
        self.sprites = [pg.image.load(f'../assets/lives/lives{i}.png') for i in range(6)]
        self.image = self.sprites[val]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def is_alive(self):
        return self.lives != 0

    def decrease_moves(self):
        if self.is_alive():
            self.lives -= 1

    def update(self):
        self.image = self.sprites[self.lives]