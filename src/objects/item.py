#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame as pg

class Item(pg.sprite.Sprite):
    """Base class for item s on the floor."""

    def __init__(self, sprite_route, floor):
        super().__init__()
        self.image = pg.image.load(sprite_route)
        self.rect = self.image.get_rect()
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.place_on_floor(floor)

    def place_on_floor(self, floor):
        floor_w = floor.image.get_width()
        floor_h = floor.image.get_height()
        self.rect.x = floor.rect.x + (floor_w - self.width)/2
        self.rect.y = floor.rect.y + (floor_h - self.height)/2

    def update(self):
        pass

class Key(Item):
    """Class for the Key that opens the locked Door."""

    def __init__(self, floor):
        super().__init__('../assets/items/key.bmp', floor)
        self.name = 'K'

class Coin(Item):
    """Class for the Coin that gives moves."""

    def __init__(self, val: int, floor):
        super().__init__(f'../assets/items/coin{val}.bmp', floor)
        self.name = 'C'
        self.value = val        

class Trapdoor(Item):
    """Class for the Trapdoor on the floor."""

    def __init__(self, floor):
        super().__init__('../assets/items/opened_trapdoor.bmp', floor)
        self.name = 'T'
        self.is_active = True
        self.sprites = {
            'opened': pg.image.load('../assets/items/opened_trapdoor.bmp'),
            'closed': pg.image.load('../assets/items/closed_trapdoor.bmp'),
        }
    
    def activate(self):
        self.is_active = True

    def deactivate(self):
        self.is_active = False

    def update(self):
        if self.is_active:
            self.image = self.sprites['opened']
        else:
            self.image = self.sprites['closed']

class Button(Item):
    """Class for the Button that interacts with the Trapdoor(s)."""

    def __init__(self, floor):
        super().__init__('../assets/items/button.bmp', floor)
        self.name = 'b'
        self.trapdoors = []
        self.is_pressed = False

    def link_trapdoors(self, trapdoors):
        self.trapdoors = trapdoors

    def press(self):
        for t in self.trapdoors:
            t.deactivate()
        
    def release(self):
        for t in self.trapdoors:
            t.activate()

    
    