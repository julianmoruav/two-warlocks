#!/usr/bin/python3

from game.level_handler import *
from game.simulator import *

import pygame as pg
from pygame.locals import *
import sys

def main():
    # Window initialization:
    pg.init()
    size = (width, height) = (1000, 600)
    screen = pg.display.set_mode(size, pg.SCALED)
    pg.display.set_caption('Two Warlocks')
    background = pg.Surface(screen.get_size())
    background = background.convert()
    background.fill((0, 0, 0))
    screen.blit(background, (0, 0))

    # Level initialization
    level = LevelHandler()
    alice = level.instances["characters"][0]
    bob = level.instances["characters"][1]
    drawables = [level.sprite_groups["land"],
                 level.sprite_groups["objects"],
                 level.sprite_groups["enemies"],
                 level.sprite_groups["characters"]]

    for o in level.instances["objects"]:
        if o.name == "b":
            o.press()
            break

    # Simulator
    sim = Simulator(level.instances)
    drawables[0].add(sim.life)

    # update the whole screen
    pg.display.update(screen)

    # Main game loop:
    while True:
        for event in pg.event.get():
            if event.type == QUIT:
                pg.quit()
                sys.exit()
            elif (event.type == pg.KEYDOWN) and (not alice.is_moving) and (not bob.is_moving):
                # alice
                if event.key == K_a:
                    sim.move_character(alice, 'left')
                elif event.key == K_d:
                    sim.move_character(alice, 'right')
                elif event.key == K_w:
                    sim.move_character(alice, 'up')
                elif event.key == K_s:
                    sim.move_character(alice, 'down')
                # bob
                if event.key == K_LEFT:
                    sim.move_character(bob, 'left')
                elif event.key == K_RIGHT:
                    sim.move_character(bob, 'right')
                elif event.key == K_UP:
                    sim.move_character(bob, 'up')
                elif event.key == K_DOWN:
                    sim.move_character(bob, 'down')
                # next level
                if event.key == K_RETURN:
                    level.next_level()
                    sim.set_instances(level.instances)
                    sim.life.lives = 3
                    alice = level.instances["characters"][0]
                    bob = level.instances["characters"][1]
                    drawables = [level.sprite_groups["land"],
                                level.sprite_groups["objects"],
                                level.sprite_groups["enemies"],
                                level.sprite_groups["characters"]]
                    drawables[0].add(sim.life)
                if event.key == K_r:
                    level.reset_level()
                    sim.set_instances(level.instances)
                    sim.life.lives = 3
                    sim.zombie_energy = 0
                    alice = level.instances["characters"][0]
                    bob = level.instances["characters"][1]
                    drawables = [level.sprite_groups["land"],
                                level.sprite_groups["objects"],
                                level.sprite_groups["enemies"],
                                level.sprite_groups["characters"]]
                    drawables[0].add(sim.life)

        # Simulate world
        if not alice.is_moving and not bob.is_moving:
            sim.move_zombies()
            sim.check_collisions()
            sim.check_lives()

        # Drawing
        screen.blit(background, (0, 0))
        for d in drawables:
            d.update()
            d.draw(screen)
        pg.display.flip()
        pg.time.delay(75)

        # check win/loss conditions
        if sim.won:
            sim.won = False
            level.next_level()
            sim.set_instances(level.instances)
            sim.life.lives = 3
            sim.zombie_energy = 0
            alice = level.instances["characters"][0]
            bob = level.instances["characters"][1]
            drawables = [level.sprite_groups["land"],
                        level.sprite_groups["objects"],
                        level.sprite_groups["enemies"],
                        level.sprite_groups["characters"]]
            drawables[0].add(sim.life)
            pg.time.delay(500)
        elif sim.lost:
            sim.lost = False
            level.reset_level()
            sim.set_instances(level.instances)
            sim.life.lives = 3
            sim.zombie_energy = 0
            alice = level.instances["characters"][0]
            bob = level.instances["characters"][1]
            drawables = [level.sprite_groups["land"],
                        level.sprite_groups["objects"],
                        level.sprite_groups["enemies"],
                        level.sprite_groups["characters"]]
            drawables[0].add(sim.life)
            pg.time.delay(1000)

if __name__ == '__main__':
    main()
