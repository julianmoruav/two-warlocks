from game.globals import TILE_SIZE
from objects.wall import Wall
from objects.door import Door
from objects.life import Life
from objects.item import *
import pygame as pg

class Probe:
    """ Class for a probe meant to inspect the surroundings to anticipate collisions"""
    def __init__(self, pos, dimensions):
        super().__init__()
        self.image = None
        self.rect = pg.Rect(pos, dimensions)

# Helper function to get the first element of a list if there is one.
def first_of(l):
    if len(l) == 0:
        return None
    else:
        return l[0]

class Simulator:
    """ Class for handling all interactions between instances on the level"""

    def __init__(self, instances):
        self.set_instances(instances)
        self.life = Life(0, 0)
        self.zombie_energy = 0
        self.won = False
        self.lost = False

    def set_instances(self, instances):
        self.land = instances["land"]
        self.objects = instances["objects"]
        self.characters = instances["characters"]
        self.enemies = instances["enemies"]

    def unlock_door(self):
        for i in self.land:
            if type(i) == Door:
                i.unlock()

    def move_character(self, character, direction):
        anticipated_colliders = self.anticipate_colliders(character, direction)
        if self.is_move_legal(anticipated_colliders):
            character.move(direction)
            self.life.decrease_moves()

            # update zombie energy for every player turn
            if self.zombie_energy != 3:
                self.zombie_energy += 1

    def is_move_legal(self, colliders_dict):
        # check lives
        if not self.life.is_alive():
            return False

        # character cannot go over other characters
        if colliders_dict["characters"] is not None:
            return False

        i = colliders_dict["land"]
        # character cannot go through walls
        if type(i) == Wall:
            return False

        # character cannot go through locked doors
        if type(i) == Door:
            return (not i.is_locked)

        # if none of the above then it is a legal move 
        return True

    def anticipate_colliders(self, character, direction):
        neighbors = character.get_neighbor_coords()
        dimensions = (TILE_SIZE/2, TILE_SIZE/2)
        probe = Probe(neighbors[direction], dimensions)

        # there is only one collision per category at most, so it is safe to use first_of
        l_collider = first_of(pg.sprite.spritecollide(probe, self.land, False))
        o_collider = first_of(pg.sprite.spritecollide(probe, self.objects, False))
        c_collider = first_of(pg.sprite.spritecollide(probe, self.characters, False))
        e_collider = first_of(pg.sprite.spritecollide(probe, self.enemies, False))

        return {"land": l_collider, "objects": o_collider, "characters": c_collider, "enemies": e_collider}

    def check_collisions(self):
        self.release_button()
        for zombie in self.enemies:
            colliders_dict = self.current_colliders(zombie)
            obj = colliders_dict["objects"]
            if type(obj) == Button:
                obj.press()

        for character in self.characters:
            # get current collisions:
            colliders_dict = self.current_colliders(character)

            # check collision against door:
            if type(colliders_dict["land"]) == Door:
                if not colliders_dict["land"].is_locked:
                    self.won = True
                    return

            # check collision against enemy:
            if colliders_dict["enemies"] is not None:
                character.die()
                self.lost = True
                return

            # check collision against other objects
            obj = colliders_dict["objects"]
            if type(obj) == Button:
                obj.press()

            if type(obj) == Key:
                self.unlock_door()
                obj.kill()
                self.objects.remove(obj)

            if type(obj) == Coin:
                self.life.lives = obj.value
                obj.kill()
                self.objects.remove(obj)

            if type(obj) == Trapdoor:
                if obj.is_active:
                    character.die()
                    self.lost = True

    def current_colliders(self, character):
        l_collider = first_of(pg.sprite.spritecollide(character, self.land, False))
        o_collider = first_of(pg.sprite.spritecollide(character, self.objects, False))
        e_collider = first_of(pg.sprite.spritecollide(character, self.enemies, False))
        return {"land": l_collider, "objects": o_collider, "enemies": e_collider}
    
    def release_button(self):
        for obj in self.objects:
            if type(obj) == Button:
                pressed_button = first_of(pg.sprite.spritecollide(obj, self.characters, False))
                if pressed_button == None:
                    obj.release()

    def check_lives(self):
        p1 = self.characters[0]
        p2 = self.characters[1]
        if not self.life.is_alive() and not self.won:
            p1.die()
            p2.die()
            self.lost = True

    def move_zombies(self):
        for z in self.enemies:
            self.move_zombie(z)

    def move_zombie(self, zombie):
        if self.zombie_energy == 3:
            self.zombie_energy = 0
            direction = self.zombie_smell_direction(zombie)
            zombie.move(direction)
        
    def zombie_smell_direction(self, zombie):
        # aliases for readability
        p1 = self.characters[0]
        p2 = self.characters[1]
        p1_pos = (p1.rect.x, p1.rect.y)
        p2_pos = (p2.rect.x, p2.rect.y)
        z_pos = (zombie.rect.x, zombie.rect.y)
        
        # difference in positions
        vector_to_p1 = (p1_pos[0] - z_pos[0], p1_pos[1] - z_pos[1])
        vector_to_p2 = (p2_pos[0] - z_pos[0], p2_pos[1] - z_pos[1])

        # Manhattan distance 
        distance_to_p1 = abs(vector_to_p1[0]) + abs(vector_to_p1[1])
        distance_to_p2 = abs(vector_to_p2[0]) + abs(vector_to_p2[1])

        # Go towards closer player
        if distance_to_p1 < distance_to_p2:
            (x, y) = vector_to_p1
        else:
            (x, y) = vector_to_p2

        # Check collisions in all directions
        colliders_l = self.anticipate_colliders(zombie, "left")
        colliders_r = self.anticipate_colliders(zombie, "right")
        colliders_u = self.anticipate_colliders(zombie, "up")
        colliders_d = self.anticipate_colliders(zombie, "down")

        # Check if character is in the same row or col
        if x == 0: # same col
            if y < 0:
                if self.is_zombie_move_legal(colliders_u): return "up"
                elif self.is_zombie_move_legal(colliders_d): return "down"
            else:
                if self.is_zombie_move_legal(colliders_d): return "down"
                elif self.is_zombie_move_legal(colliders_u): return "up"
        if y == 0: # same row
            if x < 0:
                if self.is_zombie_move_legal(colliders_l): return "left"
                elif self.is_zombie_move_legal(colliders_r): return "right"
            else:
                if self.is_zombie_move_legal(colliders_r): return "right"
                elif self.is_zombie_move_legal(colliders_l): return "left"

        # check other rows and cols.
        if abs(x) < abs(y): # closer to move horizontally
            if x < 0:
                if self.is_zombie_move_legal(colliders_l): return "left"
                elif y < 0:
                    if self.is_zombie_move_legal(colliders_u): return "up"
                    elif self.is_zombie_move_legal(colliders_d): return "down"
                    else: return "right"
                else:
                    if self.is_zombie_move_legal(colliders_d): return "down"
                    elif self.is_zombie_move_legal(colliders_u): return "up"
                    else: return "left"
            else:
                if self.is_zombie_move_legal(colliders_r): return "right"
                elif y < 0 :
                    if self.is_zombie_move_legal(colliders_u): return "up"
                    elif self.is_zombie_move_legal(colliders_d): return "down"
                    else: return "left"
                else:
                    if self.is_zombie_move_legal(colliders_d): return "down"
                    elif self.is_zombie_move_legal(colliders_u): return "up"
                    else: return "left"
        else: # closer to move vertically
            if y < 0: 
                if self.is_zombie_move_legal(colliders_u): return "up"
                elif x < 0:
                    if self.is_zombie_move_legal(colliders_l): return "left"
                    elif self.is_zombie_move_legal(colliders_r): return "right"
                    else: return "down"
                else:
                    if self.is_zombie_move_legal(colliders_r): return "right"
                    elif self.is_zombie_move_legal(colliders_l): return "left"
                    else: return "down"
            else:
                if self.is_zombie_move_legal(colliders_d): return "down"
                elif x < 0:
                    if self.is_zombie_move_legal(colliders_l): return "left"
                    elif self.is_zombie_move_legal(colliders_r): return "right"
                    else: return "up"
                else:
                    if self.is_zombie_move_legal(colliders_r): return "right"
                    elif self.is_zombie_move_legal(colliders_l): return "left"
                    else: return "down"
        
    def is_zombie_move_legal(self, colliders_dict):
        # zombies cannot go over other zombies
        if colliders_dict["enemies"] is not None:
            return False

        i = colliders_dict["land"]
        # zombies cannot go through walls
        if type(i) == Wall:
            return False

        # zombies cannot go through doors
        if type(i) == Door:
            return False

        # if none of the above then it is a legal move 
        return True