# Names of the files for each of the sprites:
sprites_files = {
    'up': [
        'up_still.png',
        'up_walk1.png',
        'up_still.png',
        'up_walk2.png'
    ],
    'down': [
        'down_still.png',
        'down_walk1.png',
        'down_still.png',
        'down_walk2.png'
    ],
    'left': [
        'left_still.png',
        'left_walk.png'
    ],
    'right': [
        'right_still.png',
        'right_walk.png'
    ],
    'dead': ['dead.png']
} 

# Size of the side of a tile
TILE_SIZE=32

