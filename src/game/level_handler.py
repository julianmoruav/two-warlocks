from game.globals import TILE_SIZE
from objects.character import *
from objects.wall import Wall
from objects.door import Door
from objects.item import *
from objects.floor import Floor
from objects.life import Life

import json
import pygame as pg
from pygame.locals import *
import sys

class LevelHandler:
    """ Class for building and manipulating the level """

    def __init__(self):
        self.levels = self.parse_levels("game/levels.json")
        self.current_level = self.read_current_level()
        self.instances = self.create_instances()
        self.sprite_groups = self.create_sprite_groups()
        
    def read_current_level(self):
        file_handler = open("game/current_level.txt", "r")
        line = file_handler.readline().strip()
        file_handler.close()
        return line

    def parse_levels(self, levels_file):
        file_handler = open(levels_file, 'r')
        levels = json.load(file_handler)
        file_handler.close()
        return levels

    def print_current_map(self):
        for line in self.levels[self.current_level]["map"]:
            print(" ".join(line))

    def create_instances(self):
        # init temp variables
        land = []
        objects = []
        characters = []
        enemies = []
        matrix = []
        x_pixel = 100
        y_pixel = 100

        # iterate the map to create the objects
        for line in self.levels[self.current_level]["map"]:
            row = []
            for symbol in line:
                if   symbol == "X":
                    w = Wall(x_pixel, y_pixel, False)
                    land.append(w)
                    row.append(w)
                elif symbol == "W":
                    w = Wall(x_pixel, y_pixel, True)
                    land.append(w)
                    row.append(w)
                elif symbol == "O":
                    d = Door(x_pixel, y_pixel, False)
                    land.append(d)
                    row.append(d)
                elif symbol == "L":
                    d = Door(x_pixel, y_pixel, True)
                    land.append(d)
                    row.append(d)

                elif symbol == "A":
                    land.append(Floor(x_pixel, y_pixel))  
                    a = Alice(x_pixel, y_pixel)
                    characters.append(a)
                    row.append(a)
                elif symbol == "B":
                    land.append(Floor(x_pixel, y_pixel))  
                    b = Bob(x_pixel, y_pixel)
                    characters.append(b)
                    row.append(b)
                elif symbol == "Z":
                    land.append(Floor(x_pixel, y_pixel))  
                    z = Zombie(x_pixel, y_pixel)
                    enemies.append(z)
                    row.append(z)

                elif symbol == "3":
                    f = Floor(x_pixel, y_pixel)
                    land.append(f)
                    c = Coin(3, f)
                    objects.append(c)
                    row.append(c)
                elif symbol == "5":  
                    f = Floor(x_pixel, y_pixel)
                    land.append(f)
                    c = Coin(5, f)
                    objects.append(c)
                    row.append(c)
                elif symbol == "K":
                    f = Floor(x_pixel, y_pixel)
                    land.append(f)
                    k = Key(f)
                    objects.append(k)
                    row.append(k)
                elif symbol == "F":
                    f = Floor(x_pixel, y_pixel)
                    land.append(f)
                    row.append(f)

                elif symbol == "T":
                    f = Floor(x_pixel, y_pixel)
                    land.append(f)
                    t = Trapdoor(f)
                    objects.append(t)
                    row.append(t)
                elif symbol == "b":
                    f = Floor(x_pixel, y_pixel)
                    land.append(f)
                    b = Button(f)
                    objects.append(b)
                    row.append(b)

                # advance in X dimension
                x_pixel += TILE_SIZE

            # reset X dimension and advance Y dimension
            matrix.append(row)
            x_pixel = 100
            y_pixel += TILE_SIZE

        # guarantee that Alice is the first character in the list and Bob the second
        if characters[0].name is not "A":
            characters = [characters[1], characters[0]]

        # Collect all trapdoors
        # This leverages the fact that copying objects actually copies a references to it
        try:
            links_list = self.levels[self.current_level]["button-links"]
            button_links = []
            for button in links_list:
                trapdoors = []
                for trap_coords in button:
                    x = trap_coords["x"]
                    y = trap_coords["y"]
                    if matrix[x][y].name == "T":
                        trapdoors.append(matrix[x][y])
                    else:
                        print("Malformed button-links list points to non-trapdoor object")
                button_links.append(trapdoors)

            # Iterate over all objects to find buttons and link the trapdoors
            current = 0
            for o in objects:
                if o.name == "b":
                    o.link_trapdoors(button_links[current])
                    current += 1
        except:
            pass

        # collect instances in a dict and return
        return {"land":land, "objects": objects, "characters": characters, "enemies": enemies}

    def create_sprite_groups(self):
        # Create groups
        land = pg.sprite.Group()
        objects = pg.sprite.Group()
        characters = pg.sprite.Group()
        enemies = pg.sprite.Group()

        # Add instances to the groups
        land.add(self.instances["land"])
        objects.add(self.instances["objects"])
        characters.add(self.instances["characters"])
        enemies.add(self.instances["enemies"])

        # Collect them in a dict and return
        return {"land":land, "objects": objects, "characters": characters, "enemies": enemies}

    def next_level(self):
        next_level = int(self.current_level) + 1
        self.change_level(next_level)

    def reset_level(self):
        next_level = int(self.current_level)
        self.change_level(next_level)
        
    def change_level(self, num):
        # If max level loop back to first level
        next_level = num
        if next_level == 10: 
            next_level = 1

        # Update current level
        self.current_level = str(next_level)
        file_handler = open("game/current_level.txt", "w")
        file_handler.write(self.current_level)
        file_handler.close()

        # Cleanup
        for key in self.sprite_groups:
            self.sprite_groups[key].empty()

        # Update instances and groups
        self.instances = self.create_instances()
        self.sprite_groups = self.create_sprite_groups()