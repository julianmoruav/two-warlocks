# Two Warlocks

A clone of the [Warlock's Tower](https://wt.midipixel.com) game by Midipixel. 


### Motivation
Two Warlocks started as a group project in college with @joselp when we were first introduced to Python. As you could imagine, the original code we wrote was terrible, but I liked the project and I learned a lot from it! Also I befriended an excellent partner for future courses. Years later, I revisited the project and decided it was worth showcasing, but I did not like the quality of the code at all. That's why I created a new repository and re-designed everything, but this time using better coding practices. 

## Run the Game

* Install pygame dependency

        python3 -m pip install -U pygame --user

* Run the game! (Linux or Mac OS)

        ./lets-play

## How to Play

It is pretty simple. Alice and Bob are trying to escape some dungeons. Alice is controlled with the A-W-S-D keys and Bob with the Arrow keys. The goal of each level is for Alice **or** Bob to reach the door and exit the level onto the next one. Hold on! Don't go running recklessly, your steps are limited. At the top left corner of the window sits your step counter. For every step that Alice **or** Bob take the counter will go down by one... if the counter reaches zero you are done! Have fun :)

* Restart the level with the `R` key. 
* Skip a level entirely with the `Return` key.
* Simply close the window when you are done playing. Run the game again and it will pick up where you left off.
* You can create your own levels by adding them to the `src/game/levels.json` file.

## Some Tips!

* Play with another person. Collaborating with someone makes it more fun!
* The progressively increasing difficulty of the levels will teach you how to play. Give them a try... or ten.
* You don't need to collect all coins, don't be greedy!
* Locked doors won't open unless Alice **or** Bob have found the key.
* Beware of trapdoors! Sit on the button that closes them before your partner walks over them.
* Did I mention there are hangry zombies ready to eat some brains? They are very clumsy so they'll only take one step after three player-moves. Don't get comfortable though, they use the extra time to take a well-thought step towards their next meal. 


